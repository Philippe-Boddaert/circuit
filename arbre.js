const DOOR = {
  0: "NOT",
  3: "AND",
  6: "OR"
}

const TYPE = {
  'LIGHT': 1,
  'BUTTON': 2,
  'PORTE_NOT': 0,
  'PORTE_AND': 3,
  'PORTE_OR': 6,
  'from_value' : function(valeur){
    let type = null;
    switch(valeur){
      case 0:
        type = 'PORTE_NOT';
        break;
      case 1:
        type = 'LIGHT';
        break;
      case 2:
        type = 'BUTTON';
        break;
      case 3:
        type = 'PORTE_AND';
        break;
      case 6:
        type = 'PORTE_OR';
    }
    return type;
  }
}

const STYLE = {
  "LIGHT": {
    "TOP" : 50,
    "WIDTH" : 100,
    "HEIGHT" : 50
  },
  "DOOR": {
    "WIDTH" : 35,
    "HEIGHT" : 25
  },
  "BUTTON": {
    "WIDTH" : 10
  }
}
function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function operation(type, gauche, droite){
    let valeur = null;
    switch(type){
      case TYPE.PORTE_NOT:
        valeur = !gauche;
        break;
      case TYPE.PORTE_AND:
        valeur = gauche && droite;
        break;
      case TYPE.PORTE_OR:
        valeur = gauche || droite;
        break;
    }
    return valeur;
}

function genererBouton(document, arbre, parent, decalage, callback){
  let g = document.createElementNS("http://www.w3.org/2000/svg", 'g');
  let newElement = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
  let x = parent.x.baseVal.value + parent.width.baseVal.value / 2 + (decalage * 2 * STYLE.BUTTON.WIDTH);
  let y = parent.y.baseVal.value + parent.height.baseVal.value + 50;
  newElement.setAttributeNS(null, 'cx', x);
  newElement.setAttributeNS(null, 'cy', y);
  newElement.setAttributeNS(null, 'r', STYLE.BUTTON.WIDTH);
  newElement.setAttributeNS(null, 'style', 'fill: ' + (arbre.valeur?'#0f0':'black') + '; stroke: #128736; stroke-width: 2px;' );

  let click = function(){
    arbre.valeur = !arbre.valeur;
    callback();
  };

  newElement.addEventListener('click', click);

  newElement.addEventListener('mouseover', function(event){
    event.target.style.cursor = "pointer";
  });
  let text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
  text.textContent = arbre.label;
  text.setAttributeNS(null, 'x', x - 3);
  text.setAttributeNS(null, 'y', y + 3);
  text.setAttributeNS(null, 'font-family', "Verdana");
  text.setAttributeNS(null, 'font-size', 12);
  text.setAttributeNS(null, 'style', 'fill:' + (arbre.valeur?'black':'#0f0') + ';');

  text.addEventListener('mouseover', function(event){
    event.target.style.cursor = "pointer";
  });
  text.addEventListener('click', click);

  g.appendChild(newElement);
  g.appendChild(text);
  return g;
};

function genererSun(document){
  let path = document.createElementNS("http://www.w3.org/2000/svg", 'path');

  path.setAttributeNS(null, 'd', "m94.480316 28.102362l-7.8611145 2.795599l0 -5.591198zm-5.7070084 -13.775107l-3.5808487 7.536168l-3.9535217 -3.9535198zm-13.773308 -5.705207l2.795601 7.8611164l-5.591202 0zm-13.775108 5.705207l7.536171 3.5826483l-3.9535217 3.9535198zm-5.705208 13.775107l7.8611183 -2.795599l0 5.591198zm5.705208 13.773306l3.5826492 -7.5343666l3.9535217 3.9535217zm13.775108 5.7070084l-2.795601 -7.8611145l5.591202 0zm13.773308 -5.7070084l-7.5343704 -3.5808449l3.9535217 -3.9535217zm-23.513466 -13.773306l0 0c0 -5.379339 4.360817 -9.740156 9.740158 -9.740156c5.379341 0 9.740158 4.360817 9.740158 9.740156c0 5.379339 -4.360817 9.740156 -9.740158 9.740156c-5.379341 0 -9.740158 -4.360817 -9.740158 -9.740156z");
  path.setAttributeNS(null, 'style', 'fill: yellow;');

  return path;
};

function genererLight(document, arbre){
  let light = document.createElementNS("http://www.w3.org/2000/svg", 'rect');

  light.setAttributeNS(null, 'x', 300 / 2 - 50);
  light.setAttributeNS(null, 'y', 50);
  light.setAttributeNS(null, 'width', STYLE.LIGHT.WIDTH);
  light.setAttributeNS(null, 'height', STYLE.LIGHT.HEIGHT);
  light.setAttributeNS(null, 'rx', 5);
  light.setAttributeNS(null, 'ry', 5);
  light.setAttributeNS(null, 'style', 'fill: ' + (arbre.valeur?'yellow':'black') + '; stroke: #827c80; stroke-width: 2px;' );

  return light;
};

function genererPatte(document, porte, decalage){
  let patte = document.createElementNS("http://www.w3.org/2000/svg", 'line');
  patte.setAttributeNS(null, 'x1', porte.x.baseVal.value + decalage * porte.width.baseVal.value);
  patte.setAttributeNS(null, 'x2', porte.x.baseVal.value + decalage * porte.width.baseVal.value);
  patte.setAttributeNS(null, 'y1', porte.y.baseVal.value + porte.height.baseVal.value);
  patte.setAttributeNS(null, 'y2', porte.y.baseVal.value + porte.height.baseVal.value + 7);
  patte.setAttributeNS(null, 'style', 'stroke: #827c80; stroke-width: 2px;');

  return patte;
}

function genererPorte(document, arbre, parent, decalage){

  let g = document.createElementNS("http://www.w3.org/2000/svg", 'g');
  let porte = document.createElementNS("http://www.w3.org/2000/svg", 'rect');

  porte.setAttributeNS(null, 'x', parent.x.baseVal.value + parent.width.baseVal.value / 2 + (decalage * STYLE.DOOR.WIDTH) - STYLE.DOOR.WIDTH / 2);
  porte.setAttributeNS(null, 'y', 50 + parent.y.baseVal.value + parent.height.baseVal.value);
  porte.setAttributeNS(null, 'width', STYLE.DOOR.WIDTH);
  porte.setAttributeNS(null, 'height', STYLE.DOOR.HEIGHT);
  porte.setAttributeNS(null, 'rx', 5);
  porte.setAttributeNS(null, 'ry', 5);
  porte.setAttributeNS(null, 'style', 'fill:black; stroke: #827c80; stroke-width: 2px; stroke-dasharray: 5;' );

  let gate = document.createElementNS("http://www.w3.org/2000/svg", 'line');
  gate.setAttributeNS(null, 'x1', porte.x.baseVal.value + porte.width.baseVal.value / 2);
  gate.setAttributeNS(null, 'x2', porte.x.baseVal.value + porte.width.baseVal.value / 2);
  gate.setAttributeNS(null, 'y1', porte.y.baseVal.value);
  gate.setAttributeNS(null, 'y2', porte.y.baseVal.value - 7);
  gate.setAttributeNS(null, 'style', 'stroke: #827c80; stroke-width: 2px;');

  let pattes = Array();
  if (arbre.type == TYPE.PORTE_NOT){
    pattes.push(genererPatte(document, porte, 1 / 2));
  } else {
    pattes.push(genererPatte(document, porte, 1 / 3));
    pattes.push(genererPatte(document, porte, 2 / 3));
  }

  let text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
  text.textContent = DOOR[arbre.type];
  text.setAttributeNS(null, 'x', (porte.x.baseVal.value + STYLE.DOOR.WIDTH / 2) - (text.textContent.length * 5));
  text.setAttributeNS(null, 'y', porte.y.baseVal.value + 4 + STYLE.DOOR.HEIGHT / 2);
  text.setAttributeNS(null, 'font-family', "Verdana");
  text.setAttributeNS(null, 'font-size', 12);
  text.setAttributeNS(null, 'style', "fill:yellow;");

  g.appendChild(porte);
  g.appendChild(gate);
  pattes.forEach((patte, i) => {
    g.appendChild(patte);
  });
  g.appendChild(text);

  return g;
}

function genererFil(document, arbre, parent, fils, decalage){
  let path = document.createElementNS("http://www.w3.org/2000/svg", 'path');
  let x1 = parent.x.baseVal.value + ((3 + decalage) * parent.width.baseVal.value) / 6;
  let y1 = parent.y.baseVal.value + parent.height.baseVal.value + 7;
  let x2 = (fils.nodeName == 'circle')?fils.cx.baseVal.value:(fils.x.baseVal.value+fils.width.baseVal.value / 2);
  let y2 = (fils.nodeName == 'circle')?(fils.cy.baseVal.value - fils.r.baseVal.value):fils.y.baseVal.value - 6;
  let y3 = y1 + ((y2 - y1) / 2);
  path.setAttributeNS(null, 'd', 'M' + x1 + ' ' + y1 + ' V ' + (y3 - 5) + ' H ' + x2 + ' V ' + y2);
  path.setAttributeNS(null, 'style', 'fill: transparent;stroke: ' + ((arbre.valeur)?'yellow':'#00ffff') + '; stroke-width: 2px; stroke-linejoin:round; stroke-dasharray:4;');

  return path;
}

class Arbre {
  constructor(label, valeur, type=TYPE.BUTTON, gauche= null, droite= null) {
    this.label = label;
    this.valeur = valeur;
    this.type = type;
    this.gauche = gauche;
    this.droite = droite;
    this.element = null;
  }

  static from_file(fichier){
    const lignes = fichier.split('\n');
    let nodes = {};
    let first = null;
    lignes.forEach((ligne, i) => {
      if (ligne[0] != '#' && ligne.length > 0){
        if (i == 1){
          let _nodes = ligne.split(' ');
          _nodes.forEach((node, i) => {
            let definition = node.split('|');
            let name = definition[0];
            let type = definition[1];
            let valeur = null;
            if (type.length > 1){
              let chaine = type.split(",");
              type = chaine[0];
              valeur = (chaine[1] == 1);
            }
            if (i == 0){
              first = name;
            }
            nodes[name] = new Arbre(name, valeur, parseInt(type));
          });
        } else {
          let _nodes = ligne.split(' ');
          let start = _nodes[0];
          let end = _nodes[1];
          nodes[start].set_fils(nodes[end]);
        }
      }
    });
    return [nodes, first];
  }

  est_feuille(){
    return this.type == TYPE.BUTTON;
  }

  est_porte(){
    return this.type % 3 == 0;
  }

  est_light(){
    return this.type == TYPE.LIGHT;
  }

  set_gauche(arbre){
    this.gauche = arbre;
  }

  set_droite(arbre){
    this.droite = arbre;
  }

  set_fils(arbre){
    if (this.gauche == null){
      this.gauche = arbre;
    } else {
      this.droite = arbre;
    }
  }

  evaluer(){
    if (this.est_porte()){
      let gauche = (this.gauche == null)?null:this.gauche.evaluer();
      let droite = (this.droite == null)?null:this.droite.evaluer();
      this.valeur = operation(this.type, gauche, droite);
    } else if (this.est_light()){
      this.valeur = this.gauche.evaluer();
    }
    return this.valeur;
  }

  afficher(document, SVG, nodes, callback, decalage = 0, parent = null){

    if (this.est_feuille()){
      if (this.element == null){
        let newElement = genererBouton(document, this, parent, decalage, callback);
        SVG.appendChild(newElement);
        this.element = newElement.children[0];
      }
      SVG.appendChild(genererFil(document, this, parent, this.element, decalage));

    } else {
      if (this.est_light()){
        removeAllChildNodes(SVG);
        for (let key of Object.keys(nodes)) {
          nodes[key].element = null;
        }
        if (this.element == null){
          let newElement = genererLight(document, this);
          //SVG.appendChild(genererSun(document));
          SVG.appendChild(newElement);
          this.element = newElement;
        }
      } else {
        if (this.element == null){
          let newElement = genererPorte(document, this, parent, decalage);
          SVG.appendChild(newElement);
          this.element = newElement.children[0];
        }
      }
      if (parent != null)
        SVG.appendChild(genererFil(document, this, parent, this.element, decalage));

      if (this.gauche != null){
        let decalage = (this.droite != null)?((this.droite == this.gauche)?0:-1):0;
        this.gauche.afficher(document, SVG, nodes, callback, decalage, this.element);
      }
      if (this.droite != null){
        this.droite.afficher(document, SVG, nodes, callback, 1, this.element);
      }
    }
  }
}
